import React from "react";
import { Jumbotron, Button } from "reactstrap";
import { Redirect } from "react-router-dom";
import { async } from "q";

export default class UserShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      redirect: false
    };
  }

  componentDidMount = async () => {
    await fetch(
      `https://arullcrud.herokuapp.com/user/${this.props.match.params.id}`
    )
      .then(response => response.json())
      .then(data => {
        console.log(data, "ini data");
        if (data.success) {
          this.setState({
            user: data.data
          });
        } else {
          this.setState({
            redirect: true
          });
        }
      });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <Jumbotron>
          <h1 className="display-3">Hello, {this.state.user.username}</h1>
          <p className="lead">Right now you are login into BINAR AUT website</p>
          <hr className="my-2" />
          <p>You can create, adding, and delete the product</p>
          <p className="lead">
            <Button color="primary">Check Your Product</Button>
          </p>
        </Jumbotron>
      </div>
    );
  }
}
