import React, { createRef } from "react";
import { Button, Col, Form, FormGroup, Label, Input } from "reactstrap";
import { Redirect } from "react-router-dom";

var dropzoneRef = createRef();

export default class Reviews extends React.Component {
  // This function does the uploading to cloudinary
  constructor(props) {
    super(props);
    this.state = {
      review: ""
    };

    this.onChangeReviews = this.onChangeReviews.bind(this);
  }

  createReviews(event) {
    event.preventDefault();
    fetch("https://arullcrud.herokuapp.com/review", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("TOKEN")
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrer: "no-referrer", // no-referrer, *client
      body: JSON.stringify({
        review: this.state.review
      }) // body data type must match "Content-Type" header
    })
      .then(response => response.json())
      .then(data => {
        console.log("Review Created. Thank You for reviewing", data);
        this.props.history.push("/");
      });
  }
  onChangeReviews(event) {
    this.setState({
      review: event.target.value
    });
  }

  render() {
    return (
      <Form>
        <FormGroup row>
          <Label for="reviews" sm={2}>
            Review
          </Label>
          <Col sm={10}>
            <Input
              type="text"
              name="review"
              id="namaBarang"
              placeholder="Please write some review to product that you already bought..."
            />
          </Col>
        </FormGroup>

        <FormGroup check row>
          <Col sm={{ size: 10, offset: 2 }}>
            <Button>Submit Reviews</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
