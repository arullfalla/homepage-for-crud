import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from "./User/Login.js";
import SignUp from "./User/SignUp.js";
import UserShow from "./User/UserShow.js";
import UserAll from "./User/UserAll.js";
import Header from "./Header.js";
import ProductCreate from "./Product/ProductCreate.js";
import ProductShow from "./Product/ProductShow.js";
import Reviews from "./User/Reviews.js";
import Home from "./Home/Home.js";
import "./App.css";

function App() {
  return (
    <div>
      <Router>
        <Header />
        <Route exact path="/">
          <body>
            <p>Welcome to CRUD web.</p>
            <br></br>
            <p>
              This website contain create, retrieve, update, and delete method
              from the BackEnd to FrontEnd.
            </p>
            <hr></hr>
            <Home></Home>
          </body>
        </Route>
        <Route exact path="/login" component={Login}></Route>
        <Route exact path="/signup" component={SignUp}></Route>
        <Route exact path="/user/:id" component={UserShow}></Route>
        <Route exact path="/user" component={UserAll}></Route>
        <Route exact path="/show-product/" component={ProductShow}></Route>
        <Route exact path="/create-product/" component={ProductCreate}></Route>
        <Route exact path="/reviews" component={Reviews}></Route>
      </Router>
    </div>
  );
}

export default App;
