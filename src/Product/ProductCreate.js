import React, { createRef } from "react";
import { Button, Col, Form, FormGroup, Label, Input } from "reactstrap";
import { Redirect, Link } from "react-router-dom";
import ReactDOM from "react-dom";
import Dropzone from "react-dropzone";
import axios from "axios";
import ProductShow from "./ProductShow";

var dropzoneRef = createRef();

export default class ProductCreate extends React.Component {
  // This function does the uploading to cloudinary
  constructor(props) {
    super(props);
    this.state = {
      gambar: "",
      name: "",
      price: ""
    };
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.createProduct = this.createProduct.bind(this);
  }

  createProduct(event) {
    event.preventDefault();
    fetch(
      `https://arullcrud.herokuapp.com/product/${this.props.match.params.id}`,
      {
        method: "PUT", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("TOKEN")
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify({
          name: this.state.name,
          price: this.state.price,
          image: this.state.image
        }) // body data type must match "Content-Type" header
      }
    )
      .then(response => response.json())
      .then(data => {
        console.log("PRODUCT CREATED", data);
        this.props.history.push("/");
      });
  }
  onChangeName(event) {
    this.setState({
      name: event.target.value
    });
  }
  onChangePrice(event) {
    this.setState({
      price: event.target.value
    });
  }

  handleUploadImages = images => {
    // uploads is an array that would hold all the post methods for each image to be uploaded, then we'd use axios.all()
    const uploads = images.map(image => {
      // our formdata
      const formData = new FormData();
      formData.append("file", image);
      formData.append("tags", "productImage"); // Add tags for the images - {Array}
      formData.append("upload_preset", "arullloginreact"); // Replace the preset name with your own
      formData.append("api_key", "278787436898751"); // Replace API key with your own Cloudinary API key
      formData.append("timestamp", (Date.now() / 1000) | 0);

      // Replace cloudinary upload URL with yours
      return axios
        .post(
          "https://api.cloudinary.com/v1_1/arullfalla/image/upload",
          formData,
          { headers: { "X-Requested-With": "XMLHttpRequest" } }
        )
        .then(response => {
          console.log(response.data);
          if (response) {
            this.setState({
              gambar: response.data.secure_url
            });
          }
        });
    });

    // We would use axios .all() method to perform concurrent image upload to cloudinary.
    axios.all(uploads).then(() => {
      // ... do anything after successful upload. You can setState() or save the data
      console.log("Images have all being uploaded");
    });
  };
  render() {
    var imageUploaded;
    if (this.state.gambar !== "") {
      imageUploaded = (
        <img style={{ width: 100 }} src={this.state.gambar} alt="" srcSet="" />
      );
    } else {
      imageUploaded = (
        <img
          style={{ width: 100 }}
          src="https://image.flaticon.com/icons/png/512/3/3901.png"
          alt=""
          srcSet=""
        />
      );
    }

    if (localStorage.getItem("isLoggedIn")) {
      return (
        <Form>
          <FormGroup row>
            <Label for="namaBarang" sm={2}>
              Nama Barang
            </Label>
            <Col sm={10}>
              <Input
                type="text"
                name="namaBarang"
                id="namaBarang"
                placeholder="Nama Barang"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="namaBarang" sm={2}>
              Harga Barang
            </Label>
            <Col sm={10}>
              <Input
                type="number"
                name="harga"
                id="harga"
                placeholder="Harga Barang"
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Label for="dropzone">Upload Gambar</Label>
            <Dropzone
              id="dropzone"
              ref={dropzoneRef}
              onDrop={this.handleUploadImages}
            >
              {({ getRootProps, getInputProps }) => (
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  {imageUploaded}
                </div>
              )}
            </Dropzone>
          </FormGroup>

          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2 }}>
              <Button>Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      );
    } else {
      return <Redirect to={"/"} />;
    }
  }
}
